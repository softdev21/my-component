/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author commis
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String imge;

    public Product(int id, String name, double price, String imge) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imge = imge;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImge() {
        return imge;
    }

    public void setImge(String imge) {
        this.imge = imge;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", imge=" + imge + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "ชาเย็น1", 65, "3.jpg"));
        list.add(new Product(2, "ชาเย็น2", 65, "3.jpg"));
        list.add(new Product(3, "ชาเย็น3", 65, "3.jpg"));
        list.add(new Product(4, "โก้โก้1", 65, "2.jpg"));
        list.add(new Product(5, "โก้โก้2", 65, "2.jpg"));
        list.add(new Product(6, "โก้โก้3", 65, "2.jpg"));
        list.add(new Product(7, "ชาเขียว1", 65, "1.jpg"));
        list.add(new Product(8, "ชาเขียว2", 65, "1.jpg"));
        list.add(new Product(9, "ชาเขียว3", 65, "1.jpg"));
        return list;
    }
}
